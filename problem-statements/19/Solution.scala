import java.io.{BufferedWriter, File, FileWriter}
import scala.util.Random

/**
  * Created by vishnu on 3/14/17.
  */
object Solution {

  val random = new Random()

  def returnSelf(string: String): String = string

  def executeCode(function: (String => String)): Unit = {
    val count = scala.io.StdIn.readLine().toInt

    val result = (1 to count).map(i => {
      val input = scala.io.StdIn.readLine()
      function(input)
    })

    result.foreach(println)

  }

  def writeTestCases(t: Int, testCaseFunction: (() => (String, String))): Unit = {
    val input = new BufferedWriter(new FileWriter(new File("in.txt")))
    val output = new BufferedWriter(new FileWriter(new File("out.txt")))
    input.write(t + "\n")
    (1 to t).foreach(i => {
      val entry = testCaseFunction()
      input.write(entry._1 + "\n")
      output.write(entry._2 + "\n")
    })
    input.close()
    output.close()
  }

  def main(args: Array[String]): Unit = {

            executeCode(problem19)

//       writeTestCases(10000, problem19GenerateTestCase)

  }


  def problem19(line: String): String = try {
    Integer.toBinaryString(line.toInt).filter(_ == '1').length.toString
  } catch {
    case e: Exception => "ERROR : " + line + " :: " + e.getMessage
  }

  def problem19GenerateTestCase(): (String, String) = {
    val n = random.nextInt(65000)

    (n.toString, problem19(n.toString))
  }
}
