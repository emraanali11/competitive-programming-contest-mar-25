from random import choice, randint
from string import lowercase

number_of_test_cases = randint(1,10)
print number_of_test_cases
print "\n"
b = 0
dict= []
for a in range(number_of_test_cases):
	number_of_words = randint(1,10)
	word_dict = []
	word_dict.append(number_of_words)
	for b in range(number_of_words):
		word_len = randint(2,10)
		word = "".join(choice(lowercase) for i in range(word_len))
		word_dict.append(word)
	k_string = randint(word_len,15)
	k_word = "".join(choice(lowercase) for i in range(k_string))
	word_dict.append(k_word)
	print word_dict
	print "\n"
