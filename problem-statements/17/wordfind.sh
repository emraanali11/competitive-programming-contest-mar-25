#!/usr/bin/env bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <test-file>";
    exit;
fi

function processLine() {
    wordsToSearch=$1
    shift
    wordList=("$@");
    targetString=${wordList[$wordsToSearch]};
    idx=0;
    out='';
    while [[ $idx -lt $wordsToSearch ]]; do
        isAllFound=1
        for (( char = 0; char < ${#wordList[$idx]}; char++ )); do
            echo $targetString | grep ${wordList[$idx]:$char:1} > /dev/null;
            if [ $? -eq 1 ]; then
                isAllFound=0
                break;
            fi
        done
        if [ $isAllFound -eq 1 ]; then
            out="${out} ${wordList[$idx]}";
        fi
        idx=$((idx+1));
    done
    echo $out;
}

testFile=$1;
testCases=
{
    read testCase;
    while read line; do
        processLine $line;
    done

} < $testFile;

